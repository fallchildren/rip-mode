;;; rip-mode.el ---                                  -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Sebastian Fieber

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; Keywords: tools, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Major-mode for editing riposte files.
;;
;; From https://docs.racket-lang.org/riposte/
;;
;; "Riposte is a scripting language for evaluating JSON-bearing HTTP
;;  responses.  The intended use case is a JSON-based HTTP API.  It
;;  comes with a commandline tool, riposte, which executes Riposte
;;  scripts.  Using Riposte, one writes a sequence of commands—which
;;  amount to HTTP requests—and assertions, which require that the
;;  response meets certain conditions."

;;; Code:

(require 'seq)
(require 'pcase)
(require 'js)
(require 'compile)

(defgroup rip-mode nil
  "Major-mode for editing riposte files."
  :group 'languages)

(defcustom rip-riposte-executable "riposte"
  "Path to the riposte executable."
  :type 'string
  :safe nil
  :group 'rip-mode)

(defcustom rip-pop-to-execute-buffer t
  "If t `rip-execute-buffer' pops to the executing buffer."
  :type 'boolean
  :safe t
  :group 'rip-mode)

(defvar rip--temp-files '())

(defvar rip-mode-map
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap "\C-c\C-c" 'rip-execute-buffer)
    keymap))

(defvar rip-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?$ "_" table)
    (modify-syntax-entry ?^ "_" table)
    (modify-syntax-entry ?@ "_" table)
    (modify-syntax-entry ?% "_" table)
    (modify-syntax-entry ?/ "_" table)
    (modify-syntax-entry ?# "<" table)
    (modify-syntax-entry ?\n ">" table)
    (modify-syntax-entry ?' "\"" table)
    table))

(defvar rip-font-lock-keywords
  `(("^GET\\|^PUT\\|^POST\\|^HEAD\\|^OPTIONS\\|^DELETE" . font-lock-keyword-face)
    ("^import" . font-lock-keyword-face)
    ("^import \\(.*\\)" . (1 font-lock-type-face))
    (" with \\([0-9xX]+\\)" . (1 font-lock-constant-face))
    ("$\\([-_a-zA-Z]+\\)" . (1 font-lock-variable-name-face))
    ("%\\([-_a-zA-Z]+\\)" . (1 font-lock-function-name-face))
    ("\\^\\([-_a-zA-Z]+\\)" . (1 font-lock-type-face))
    ("\\@\\([-_a-zA-Z]+\\)" . (1 font-lock-variable-name-face))
    ("[a-z]+://[-_/a-zA-Z.&?=:0-9]+" . font-lock-type-face)
    ("\\(^GET\\|^PUT\\|^POST\\|^HEAD\\|^OPTIONS\\|^DELETE\\) \\($[-_a-zA-Z]+ \\)?\\([-_/a-zA-Z.&?=:0-9]+\\).*\\(responds\\|with headers\\)?" . (3 font-lock-type-face))
    ("{\\([-_a-zA-Z]+\\)}" . (1 font-lock-function-name-face))
    ("[/&][-_/a-zA-Z.&?=:0-9]+" . font-lock-type-face)
    ("\\b\\(true\\|false\\|null\\)\\b" . (1 font-lock-constant-face))))

(defvar rip-output-mode-font-lock-keywords
  '(("^FAIL" . font-lock-warning-face)
    ("\\(^FAIL\\|^GET\\|^PUT\\|^POST\\|^HEAD\\|^OPTIONS\\|^DELETE\\).*\\([0-9xX]\\{3\\}\\)" . (2 font-lock-constant-face))
    ("^GET\\|^PUT\\|^POST\\|^HEAD\\|^OPTIONS\\|^DELETE" . font-lock-function-name-face)))


;; rip-indent-line and rip--proper-indentation are slimmed down
;; versions of js-indent-line and js--proper-indentation
(defun rip-indent-line ()
  "Indent the current line as JavaScript."
  (interactive)
  (let* ((parse-status
          (save-excursion (syntax-ppss (point-at-bol))))
         (offset (- (point) (save-excursion (back-to-indentation) (point)))))
    (unless (nth 3 parse-status)
      (indent-line-to (rip--proper-indentation parse-status))
      (when (> offset 0) (forward-char offset)))))

(defun rip--proper-indentation (parse-status)
  "Return the proper indentation for the current line."
  (save-excursion
    (back-to-indentation)
    (cond ((nth 4 parse-status)    ; inside comment
           (js--get-c-offset 'c (nth 8 parse-status)))
          ((nth 3 parse-status) 0) ; inside string
          ((eq (char-after) ?#) 0)
          ((save-excursion (js--beginning-of-macro)) 4)
          ;; Indent array comprehension continuation lines specially.
          ((let ((bracket (nth 1 parse-status))
                 beg)
             (and bracket
                  (not (js--same-line bracket))
                  (setq beg (js--indent-in-array-comp bracket))
                  ;; At or after the first loop?
                  (>= (point) beg)
                  (js--array-comp-indentation bracket beg))))
          ((nth 1 parse-status)
           ;; A single closing paren/bracket should be indented at the
           ;; same level as the opening statement. Same goes for
           ;; "case" and "default".
           (let ((same-indent-p (looking-at "[]})]")))
             (goto-char (nth 1 parse-status)) ; go to the opening char
             (if (or (not js-indent-align-list-continuation)
                     (looking-at "[({[]\\s-*\\(/[/*]\\|$\\)")
                     (save-excursion (forward-char) (js--broken-arrow-terminates-line-p)))
                 (progn ; nothing following the opening paren/bracket
                   (skip-syntax-backward " ")
                   (when (eq (char-before) ?\)) (backward-list))
                   (back-to-indentation)
                   (js--maybe-goto-declaration-keyword-end parse-status)
                   (let* ((same-indent-p same-indent-p)
                          (indent
                           (+
                            (current-column)
                            (cond (same-indent-p 0)
                                  (t
                                   (+ js-indent-level
                                      (pcase (char-after (nth 1 parse-status))
                                        (?\( js-paren-indent-offset)
                                        (?\[ js-square-indent-offset)
                                        (?\{ js-curly-indent-offset))))))))
                     indent))
               ;; If there is something following the opening
               ;; paren/bracket, everything else should be indented at
               ;; the same level.
               (unless same-indent-p
                 (forward-char)
                 (skip-chars-forward " \t"))
               (current-column))))
          (t (prog-first-column)))))

(defun rip--cleanup-temp-files (buffer result)
  "Cleanup all temp files made by `rip-execute-buffer'.

This function is added to `compilation-finish-functions' when
entering `rip-output-mode'.

BUFFER and RESULT are ignored."
  (setq rip--temp-files
        (seq-filter (lambda (x)
                      (delete-file x nil)
                      nil)
                    rip--temp-files)))

(defun rip-execute-buffer ()
  "Run riposte with the current buffers content."
  (interactive)
  (unless (eq major-mode 'rip-mode)
    (error "Buffer not in rip-mode"))
  (save-excursion
    (let* ((temporary-file-directory ".")
           (tmp (make-temp-file "rip" nil nil (buffer-string))))
      (cl-pushnew tmp rip--temp-files)
      (compilation-start (concat rip-riposte-executable " " tmp)
                         #'rip-output-mode)
      (when rip-pop-to-execute-buffer
        (pop-to-buffer "*rip-output*")))))


;;;###autoload
(define-derived-mode rip-mode prog-mode "riposte"
  "Major-mode for editing riposte files.

\\{rip-mode-map}"
  :syntax-table rip-syntax-table
  (use-local-map rip-mode-map)
  (setq-local font-lock-defaults '(rip-font-lock-keywords))
  (setq-local indent-line-function #'rip-indent-line)
  (setq-local comment-start "# ")
  (setq-local comment-end ""))

(define-compilation-mode rip-output-mode "riposte output"
  (setq-local compilation-finish-functions '(rip--cleanup-temp-files))
  (setq-local font-lock-defaults '(rip-output-mode-font-lock-keywords)))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.rip\\'" . rip-mode))

(provide 'rip-mode)
;;; rip-mode.el ends here
